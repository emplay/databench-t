/*
 * Copyright 2022-2027 中国信息通信研究院云计算与大数据研究所
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */ 
package com.pojo;

import java.util.Date;

public class Tranlog {

    private Date tranlog_date; //交易日期
    private String tranlog_branchid; //发生网点
    private Integer tranlog_branchseq; //流水号
    private String tranlog_trancfgid; //交易码
    private Long tranlog_strtime; //开始时间
    private Long tranlog_endtime; //结束时间
    private Integer tranlog_tmcost; //交易耗时
    private String tranlog_success; //成功标志
    private String tranlog_note; //交易说明
    private String tranlog_fld1;//与processlist表process_id对应
    
    public Date getTranlog_date() {
        return tranlog_date;
    }

    public void setTranlog_date(Date tranlog_date) {
        this.tranlog_date = tranlog_date;
    }

    public String getTranlog_branchid() {
        return tranlog_branchid;
    }

    public void setTranlog_branchid(String tranlog_branchid) {
        this.tranlog_branchid = tranlog_branchid;
    }

    public Integer getTranlog_branchseq() {
        return tranlog_branchseq;
    }

    public void setTranlog_branchseq(Integer tranlog_branchseq) {
        this.tranlog_branchseq = tranlog_branchseq;
    }

    public String getTranlog_trancfgid() {
        return tranlog_trancfgid;
    }

    public void setTranlog_trancfgid(String tranlog_trancfgid) {
        this.tranlog_trancfgid = tranlog_trancfgid;
    }



    public Long getTranlog_strtime() {
		return tranlog_strtime;
	}

	public void setTranlog_strtime(Long tranlog_strtime) {
		this.tranlog_strtime = tranlog_strtime;
	}

	public Long getTranlog_endtime() {
		return tranlog_endtime;
	}

	public void setTranlog_endtime(Long tranlog_endtime) {
		this.tranlog_endtime = tranlog_endtime;
	}

	public Integer getTranlog_tmcost() {
        return tranlog_tmcost;
    }

    public void setTranlog_tmcost(Integer tranlog_tmcost) {
        this.tranlog_tmcost = tranlog_tmcost;
    }

    public String getTranlog_success() {
        return tranlog_success;
    }

    public void setTranlog_success(String tranlog_success) {
        this.tranlog_success = tranlog_success;
    }

    public String getTranlog_note() {
        return tranlog_note;
    }

    public void setTranlog_note(String tranlog_note) {
        this.tranlog_note = tranlog_note;
    }
    public Tranlog() {
    	
    }
	public Tranlog(Date tranlog_date, String tranlog_branchid, Integer tranlog_branchseq, String tranlog_trancfgid,
			Long tranlog_strtime, Long tranlog_endtime, Integer tranlog_tmcost, String tranlog_success,
			String tranlog_note,String tranlog_fld1) {
		super();
		this.tranlog_date = tranlog_date;
		this.tranlog_branchid = tranlog_branchid;
		this.tranlog_branchseq = tranlog_branchseq;
		this.tranlog_trancfgid = tranlog_trancfgid;
		this.tranlog_strtime = tranlog_strtime;
		this.tranlog_endtime = tranlog_endtime;
		this.tranlog_tmcost = tranlog_tmcost;
		this.tranlog_success = tranlog_success;
		this.tranlog_note = tranlog_note;
		this.tranlog_fld1 = tranlog_fld1;
	}

	public String getTranlog_fld1() {
		return tranlog_fld1;
	}

	public void setTranlog_fld1(String tranlog_fld1) {
		this.tranlog_fld1 = tranlog_fld1;
	}
    
    
}
