/*
 * Copyright 2022-2027 中国信息通信研究院云计算与大数据研究所
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */ 
package com.pojo;

import java.sql.Timestamp;

public class ProcessList {

	private String process_id;
	
	private String datacfg_id;
	
	private String trancfg_id;
	
	private String process_content;
	
	private String process_type;
	
	private Timestamp process_time;
	/**
	 * 进度条,取值0到100
	 */
	private int process_perc;

	public String getProcess_id() {
		return process_id;
	}

	public void setProcess_id(String process_id) {
		this.process_id = process_id;
	}

	public String getDatacfg_id() {
		return datacfg_id;
	}

	public void setDatacfg_id(String datacfg_id) {
		this.datacfg_id = datacfg_id;
	}

	public String getTrancfg_id() {
		return trancfg_id;
	}

	public void setTrancfg_id(String trancfg_id) {
		this.trancfg_id = trancfg_id;
	}


	public String getProcess_type() {
		return process_type;
	}

	public void setProcess_type(String process_type) {
		this.process_type = process_type;
	}


	public Timestamp getProcess_time() {
		return process_time;
	}

	public void setProcess_time(Timestamp process_time) {
		this.process_time = process_time;
	}

	
	public int getProcess_perc() {
		return process_perc;
	}

	public void setProcess_perc(int process_perc) {
		this.process_perc = process_perc;
	}

	public String getProcess_content() {
		return process_content;
	}

	public void setProcess_content(String process_content) {
		this.process_content = process_content;
	}

}
