/*
 * Copyright 2022-2027 中国信息通信研究院云计算与大数据研究所
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */ 
package com.controller;

import java.util.List;
import java.util.Random;

import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.common.constant.ClusterConstant;
import com.common.fastjson.JsonResult;
import com.localMapper.ClustercfgMapper;
import com.pojo.Clustercfg;

@RestController
public class ClustercfgController {

	@Autowired
	private ClustercfgMapper clustercfgMapper;

	@RequestMapping("/addClustercfg")
	@ResponseBody
	public JsonResult addClustercfg(@Param(value = "cluster_type") String cluster_type,
			@Param(value = "cluster_url") String cluster_url) {
		if(ClusterConstant.CLUSTER_MASTER.equals(cluster_type)) {
			List<Clustercfg> list = clustercfgMapper.getClustercfg();
			for(Clustercfg clustercfg:list) {
				if(ClusterConstant.CLUSTER_MASTER.equals(clustercfg.getCluster_type())) {
					return JsonResult.failure(" 集群只能有一个master节点 ");
				}
			}
		}
		Clustercfg clustercfg = new Clustercfg();
		clustercfg.setCluster_finish(ClusterConstant.SLAVE_STOP);
		clustercfg.setCluster_id(getRandomChar(4));
		clustercfg.setCluster_stat(ClusterConstant.SLAVE_FAILURE);
		clustercfg.setCluster_type(cluster_type);
		clustercfg.setCluster_url(cluster_url);
		clustercfgMapper.addClustercfg(clustercfg);
		return JsonResult.success();
	};

	
	@RequestMapping("/updateClustercfg")
	@ResponseBody
	public JsonResult updateClustercfg(@Param(value = "cluster_id") String cluster_id,
			@Param(value = "cluster_url") String cluster_url) {
		Clustercfg clustercfg = new Clustercfg();
		clustercfg.setCluster_id(cluster_id);
		clustercfg.setCluster_url(cluster_url);
		clustercfgMapper.updateClustercfg(clustercfg);
		return JsonResult.success();
	};
	
	
	@RequestMapping("/deleteClustercfg")
	@ResponseBody
	public JsonResult deleteClustercfg(@Param(value = "cluster_id") String cluster_id) {
		clustercfgMapper.deleteClustercfg(cluster_id);
		return JsonResult.success();
	};

	public static String getRandomChar(int length) { // 生成随机字符串
		char[] chr = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I',
				'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z' };
		Random random = new Random();
		StringBuffer buffer = new StringBuffer();
		for (int i = 0; i < length; i++) {
			buffer.append(chr[random.nextInt(36)]);
		}
		return buffer.toString();
	}
}
